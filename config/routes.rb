ConcreteTest::Application.routes.draw do
  root to: 'shots#index'
  resources :shots, only: [:index, :show]
end