require 'spec_helper'

feature 'User can see the popular shots' do
  scenario 'they see the shots on page' do
    returned_shots = {"shots" => [{"id" => "1", "title" => "Working on the new shop"}]}
    expect_any_instance_of(DribbleApi).to receive(:all_cached).and_return(returned_shots)

    visit root_path
    returned_shots['shots'].each do |shot|
      expect(page).to have_css "a#shot#{shot['id']}", text: shot['title']
    end
  end

  scenario 'they cant see the shots on page at this moment' do
    expect_any_instance_of(DribbleApi).to receive(:all_cached).and_return([])

    visit root_path
    expect(page).to have_css '#noshots', text: 'There are no shots at this moment'
  end
end