require 'spec_helper'

feature 'User can see a shot detailed' do
  scenario 'they see the shot detailed on page from root page' do
    returned_shot = {"id" => "1", "title" => "Working on the new shop", "description" => "<p> something </p>", "player" => {"avatar_url" => "http://avatar", "url" => "http://", "name" => "some name"}}
    returned_shots = {"shots" => [returned_shot]}
    expect_any_instance_of(DribbleApi).to receive(:all_cached).and_return(returned_shots)
    expect_any_instance_of(DribbleApi).to receive(:find_cached).and_return(returned_shot)

    returned_shots['shots'].each do |shot|
      visit root_path
      sleep 1
      click_link "shot#{shot['id']}"
      expect(page).to have_css "#shot#{shot['id']}", text: shot['name']
    end
  end

  scenario 'they see the shot detailed on page from url detail page' do
    returned_shot = {"id" => "1", "title" => "Working on the new shop", "description" => "<p> something </p>", "player" => {"avatar_url" => "http://avatar", "url" => "http://", "name" => "some name"}}
    expect_any_instance_of(DribbleApi).to receive(:find_cached).and_return(returned_shot)

    visit shot_path returned_shot['id']
    expect(page).to have_css "#shot#{returned_shot['id']}", text: returned_shot['title']
  end

  scenario 'they cant see the shot' do
    expect_any_instance_of(DribbleApi).to receive(:find_cached).and_return({})

    visit shot_path 0
    expect(page).to have_css '#noshot', text: 'This shot could not be found'
  end
end