require 'spec_helper'

describe ShotsController, type: :controller do
  it "make a request to index" do
    get :index
    expect(assigns(:shots)['shots'].count).to be > 0
    expect(response).to be_success
    expect(response).to render_template :index
  end

  context "make a request for a single shot" do
    let!(:structed_shot) { Struct.new :id, :title }
    let!(:shot) { structed_shot.new 1799501, 'Strawberryfield 2x' }

    it "make a request to show" do
      get :show, id: shot.id
      expect(assigns(:shot)['id']).to eq(shot.id)
      expect(response).to be_success
      expect(response).to render_template :show
    end
  end
end