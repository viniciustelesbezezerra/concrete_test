require 'spec_helper'

describe ApplicationHelper, type: :helper do
  it "get next page" do
    shot = {'id' => 1, 'page' => 1, 'pages' => 50}
    expect(next_page(shot)).to eq "<a href=\"/shots?page=2#nextpage\" id=\"shot1\">Next</a>"
  end

  it "dont get next page" do
    shot = {'id' => 1, 'page' => 50, 'pages' => 50}
    expect(next_page(shot)).to eq nil
  end

  it "get previous page" do
    shot = {'id' => 1, 'page' => 1, 'pages' => 50}
    expect(previous_page(shot)).to eq nil
  end

  it "dont get previous page" do
    shot = {'id' => 1, 'page' => 50, 'pages' => 50}
    expect(previous_page(shot)).to eq "<a href=\"/shots?page=49#previouspage\" id=\"shot1\">Previous</a>"

  end
end