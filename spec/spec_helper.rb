require 'simplecov'
SimpleCov.start do
  add_filter "/spec/"
  add_filter "/vendor/"
  add_filter "/tmp/"

  add_group "Models", "app/models"
  add_group "Controllers", "app/controllers"
  add_group "Helpers", "app/helpers"
  add_group "Configs", "config"
end

ENV["RAILS_ENV"] ||= 'test'

require File.expand_path("../../config/environment", __FILE__)
require 'rubygems'
require 'rspec/rails'
require 'capybara/rspec'

Capybara.run_server = true
Capybara.server_port = 8200
Capybara.default_driver = :selenium
Capybara.default_wait_time = 5

RSpec.configure do |config|
  config.color = true
  config.tty = true
  config.formatter = :documentation

  config.include Capybara::DSL, type: :request
  config.include Rails.application.routes.url_helpers
end