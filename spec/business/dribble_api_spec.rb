require 'spec_helper'

describe DribbleApi do
  let!(:dribble) { DribbleApi.new }

  context "called with good requests" do
    it "response return multiple popular shots" do
      expect(dribble.all("")['shots'].count).to be > 0
    end

    it "response return a popular shot" do
      expect(dribble.find('1757954').count).to be > 0
    end
  end

  context "called with bad requests" do
    let!(:my_json) { JSON }

    it "response dont return multiple popular shots" do
      my_json.stub(:parse).and_raise(JSON::ParserError)
      expect(dribble.all("")).to eq []
    end

    it "response dont return a popular shot" do
      my_json.stub(:parse).and_raise(JSON::ParserError)
      expect(dribble.find('1757954')).to be {}
    end
  end
end