class ShotsController < ApplicationController
  before_filter :api_instance
  caches_page :show

  def index
    @shots = @dribble_api.all_cached params[:page]
  end

  def show
    @shot = @dribble_api.find_cached params[:id]
  end

  private
  def api_instance
    @dribble_api = DribbleApi.new
  end
end