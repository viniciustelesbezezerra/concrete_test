module ApplicationHelper
  def next_page(shot)
    link_to 'Next', shots_path(anchor: "nextpage", page: shot['page'].to_i + 1), id: "shot#{shot['id']}" if has_next_page?(shot)
  end

  def previous_page(shot)
    link_to 'Previous', shots_path(anchor: "previouspage", page: shot['page'].to_i - 1), id: "shot#{shot['id']}" if has_previous_page?(shot)
  end

  private
  def has_next_page?(shot)
    shot['page'].to_i < shot['pages'].to_i
  end

  def has_previous_page?(shot)
    shot['page'].to_i <= shot['pages'].to_i && shot['page'].to_i > 1
  end
end