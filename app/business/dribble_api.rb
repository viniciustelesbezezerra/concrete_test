class DribbleApi
  def all_cached(page)
    Rails.cache.fetch("DribbleApi_all_cached_#{page}") { all(page) }
  end

  def find_cached(id)
    Rails.cache.fetch("DribbleApi_find_cached_#{id}") { find(id) }
  end

  def all(page)
    try('popular', [], {params: {page: page}})
  end

  def find(id)
    try(id, {})
  end

  private
  def try(url, return_factor, params = {})
    JSON.parse(RestClient.get('http://api.dribbble.com/shots/' + url, params)) rescue return_factor
  end
end